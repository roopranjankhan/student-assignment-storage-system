Project description : A student file storage system for uploading assignments for courses. Validations should be there in place
                      for due dates.
                      
Tech stack : Angular for frontend and Firebase for backend.

Main milestones:


a)  A daily plan with a schedule for working on the project

b)  Rigorous testing of the updates made

c)  Following clean code concepts.

d)  Finding bugs in the code and trying to fix them.

e)  Gathering feedback from others.


Link to Trello board : https://trello.com/b/c8RdcKOb/assignment-storage-system

Video Link: https://loom.com/share/15c5f86835a749368b9cb9fe9eedf1b7

Build instructions:

1) Clone the project in the IDE.

2) Make sure to have Node JS installed

3) Also install angular cli using the command:

npm install -g @angular/cli

4) Run the cloned project locally using the command:

ng serve --o



