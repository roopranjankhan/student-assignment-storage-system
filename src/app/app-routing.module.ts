import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomepageComponent} from './homepage/homepage.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {CoursepageComponent} from './coursepage/coursepage.component';
import {UploadsComponent} from './uploads/uploads.component';


const routes: Routes = [

  {
    path: '',
    component: HomepageComponent
  },

  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'sign-up',
    component: SignupComponent
  },

  {
    path: 'course',
    component: CoursepageComponent
  },

  {
    path: 'upload',
    component: UploadsComponent
  },

  {
    path: 'sign-up',
    component: SignupComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
