import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

export interface Person {
  value: number;
  viewValue: string;
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public showMyMessage = false;

  user = '';

  persons: Person[] = [
    {value: 1, viewValue: 'Student'},
    {value: 2, viewValue: 'Lecturer'},
    {value: 3, viewValue: 'Administrator'},
  ];

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  });

  constructor() {
  }

  ngOnInit() {
  }


  showMessageSoon() {
    if (this.loginForm !== null) {
      this.showMyMessage = true;
    }
  }


}
