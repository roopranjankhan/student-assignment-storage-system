import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  assignmentDetailList: AngularFireList<any>;

  constructor(private firebase: AngularFireDatabase ) { }

  getAssignmentDetailList() {
    this.assignmentDetailList = this.firebase.list('assignmentDetails');
  }

  insertAssignmentDetails(assignmentDetails) {
    this.assignmentDetailList.push(assignmentDetails);
  }
}
