import {Component, OnInit} from '@angular/core';
import {UploadService} from '../services/upload.service';

@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.css']
})
export class UploadsComponent implements OnInit {

  assignmentList: any[];
  rowIndexArray: any[];

  constructor(private service: UploadService) {
  }

  ngOnInit() {
    this.service.getAssignmentDetailList();
    this.service.assignmentDetailList.snapshotChanges().subscribe(
      list => {
        this.assignmentList = list.map(item => {
          return item.payload.val();
        });
        this.rowIndexArray = Array.from(Array(Math.ceil((this.assignmentList.length + 1) / 3)).keys());
      }
    );
  }
}
