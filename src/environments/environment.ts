// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDQuJVzN_ql7bX2cMA6qo835Z58zjgvZdY',
    authDomain: 'assignment-9e2ec.firebaseapp.com',
    databaseURL: 'https://assignment-9e2ec-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'assignment-9e2ec',
    storageBucket: 'assignment-9e2ec.appspot.com',
    messagingSenderId: '551725540145',
    appId: '1:551725540145:web:bd6977710317da18a28610'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
